export interface PointVente{
	id: Number;
	id_societe : Number;
	nom : string,
	ville : string,
	pos_gps : any,
	adresse : string,
	concurrents : string
}