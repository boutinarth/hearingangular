import { Component, OnInit } from '@angular/core';
import {PointVenteService} from '../point-vente/point-vente.service';
import {PointVente} from "../point-vente/point-vente.interface";

@Component({
  selector: 'app-ajout-point-vente',
  templateUrl: './ajout-point-vente.component.html',
  styleUrls: ['./ajout-point-vente.component.scss']
})
export class AjoutPointVenteComponent implements OnInit {

	pointvente: PointVente={
		id: null,
		id_societe: 2, //A changer
		nom: '',
		ville: '',
		pos_gps: '',
		adresse: '',
		concurrents: '',
	};

  constructor(private pointventeService: PointVenteService) { }

  ngOnInit() {
  }

  creerPointVente(data:PointVente){
  	this.pointventeService.createPointVente(data);
  }

}
