import { Component, OnInit } from '@angular/core';
import { Questionnaire } from '../questionnaire/questionnaire.interface';
import { QuestionnaireService } from '../questionnaire/questionnaire.service';
import { PointVenteService } from '../point-vente/point-vente.service';
import { PointVente } from '../point-vente/point-vente.interface';

@Component({
  selector: 'app-ajout-questionnaire',
  templateUrl: './ajout-questionnaire.component.html',
  styleUrls: ['./ajout-questionnaire.component.scss']
})
export class AjoutQuestionnaireComponent implements OnInit {

  //TODO création/stockage des questions
  
	themes = [
		'Image Prix',
		'Promotion',
		'Qualité des produits',
		'Amabilité de notre personnel',
		'Rapport Qualité/Prix des produits à notre marque',
		'Rapidité et facilité de paiement de vos achats',
		'Qualité du matériel mis à votre disposition',
		'Choix dans notre assortiment',
		'Facilité à trouver les produits',
		'Prix de nos produits Bio',
		'Qualité de nos produits Bio'
		];
	
	selection=[true,true,false,false,false,false,false,false,false,false,false];

	pointsVente : PointVente[];

	questionnaire : Questionnaire ={
		id : null,
		id_societe : 2, //a changer depuis la session
		id_pointvente : null,
		themes: '',
		questions :'',
	}

  constructor( private questionnaireService : QuestionnaireService, private pointVenteService: PointVenteService) { }

  ngOnInit() {
  	//On récupère les points de vente du client pour le sélecteur dans le formulaire
  	this.pointVenteService.getPointVente().subscribe((data:PointVente[])=>{
  		console.log(data);
  		this.pointsVente=data;
  		this.pointsVente.forEach((pointvente)=>{
  			if(pointvente.id_societe!=this.questionnaire.id_societe){
  				this.pointsVente.splice(this.pointsVente.indexOf(pointvente),1);
  			}
  		});
  		console.log("Points de vente de cette société d'id=",this.questionnaire.id_societe," :",this.pointsVente);
  		//On set le selecteur au premier point de vente de la liste
  		this.questionnaire.id_pointvente=this.pointsVente[0].id;
  	});
  }

  formattageThemes(){
  	var i=0;
  	var retour='';
  	this.selection.forEach((selected)=>{
  		if(selected){
  			retour=retour+this.themes[i]+';';
  		}
  		i++;
  	});
	this.questionnaire.themes=retour;
  }

  creerQuestionnaire(data:Questionnaire){
  	console.log("data avant envoi serveur",data);
    data.id_pointvente=Number(data.id_pointvente);
  	this.formattageThemes();
  	this.questionnaireService.createQuestionnaire(data);
    //this.questionService.createQuestions(data);
    //Création des questions dans la table questions selon le thème et avec l'id du questionnaire.
    //Penser à changer chaque champ ( Nom du point de vente, ordre des questions, valeurs des réponses possibles, qdependance)
  }

}
