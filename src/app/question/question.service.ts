import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";

import { DropdownQuestion } from './question-dropdown';
import { QuestionBase }     from './question-base';
import { TextboxQuestion }  from './question-textbox';
import { RadioQuestion } from './question-radio';

@Injectable()
export class QuestionService {

  constructor(private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  url = 'http://localhost:3000';

  //Fonction qui récupère les questions du questionnaire passé en paramètre
  getQuestions(id:number=-1) {
    console.log("getQuestions");
    if(id<0){
      console.log("Erreur, id questionnaire introuvable ou invalide");
      return Promise.reject("Erreur id questionnaire introuvable ou invalide");
    }
    else
    {
      let questionsretour: QuestionBase<any>[]=[];
      let observableQuestions=this.http.get(`${this.url}/question/${id}`);
      let questions;
      observableQuestions.toPromise().then((data)=>{
        questions=data;
        //On garde uniquement les questions de notre questionnaire.
        let index = 0;
        questions.forEach((question)=>{
          if(question.qidquestionnaire!=id){
            questions.pop(index);
          }
          index++;
        });

        //On parse le questionnaire depuis la query de la BDD
        questions.forEach((question)=>{
          console.log(question);
          if(question.qdependance!=''){
              var qdependance=JSON.parse(question.qdependance);
            }
          switch (question.qcontrolType) {

            case "radio":
              if(question.qvalue!=''){
                var qvalue=JSON.parse(question.qvalue);
              }
              questionsretour.push(
                new RadioQuestion({
                    key: question.qkey,
                    label: question.qlabel,
                    options: qvalue,
                    order: question.qorder,
                    required: question.qrequired,
                    dependance: qdependance
                  }));
              break;

            case "checkbox":
            //todo
            break;

            case "textbox":
            questionsretour.push(
              new TextboxQuestion({
                  key: question.qkey,
                  label: question.qlabel,
                  order: question.qorder,
                  required: question.qrequired,
                  dependance : qdependance
                }));
            break;
            
            default:
            console.log("question.service parsing des questions, le type de la question n'existe pas.")
              break;
          }
        });
     });
      return Promise.resolve(questionsretour);
    }
  }

  // TODO: get from a remote source of question metadata
  // TODO: make asynchronous
  /*getQuestions() {

    let questions: QuestionBase<any>[] = [

      new RadioQuestion({
        key: 'frequentation1',
        label: 'Fréquentez vous la société Carrefour Contact de Lille ?',
        options: [
          {key: 'oui',  value: 'Oui'},
          {key: 'non',  value: 'Non'}
        ],
        order: 1
      }),

      new RadioQuestion({
        key: 'frequentation2',
        label: 'A quand date votre dernière visite à la société Carrefour Contact de Lille ?',
        options: [
          {key: 'moinsunmois',  value: "Moins d'un mois"},
          {key: 'plusunmois',  value: "Plus d'un mois"}
        ],
        order: 2
      }),

      new RadioQuestion({
        key: 'frequentation3',
        label: 'Fréquentez-vous une enseigne concurrente à la société Carrefour Contact de Lille ?',
        options: [
          {key: 'auchan',  value: "Auchan"},
          {key: 'carrefour',  value: "Carrefour"}
        ],
        order: 3
      }),

    ];

    return questions.sort((a, b) => a.order - b.order);
  }*/
}