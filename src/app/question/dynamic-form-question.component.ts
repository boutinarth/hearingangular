import { Component, Input } from '@angular/core';
import { FormGroup }        from '@angular/forms';

import { QuestionBase }     from './question-base';
import {MatRadioModule} from '@angular/material/radio';
import {MatDividerModule} from '@angular/material/divider';

@Component({
  selector: 'app-question',
  templateUrl: './dynamic-form-question.component.html'
})
export class DynamicFormQuestionComponent {
  @Input() question: QuestionBase<any>;
  @Input() form: FormGroup;
  get isValid() { return this.form.controls[this.question.key].valid; };
  labeltemporaire='';

  constructor(){
  }

  ngOnInit(){
    //Lors de la réponse à une question on passe à la question suivante
  	this.form.get(this.question.key).valueChanges.subscribe((value)=>{
      //Requiert que les id des questions se suivent parfaitement ( 1 2 3 4 5 6 7 8 9 etc)
      //TODO:Problème avec questions optionnelles
  		let element = document.getElementById(String(this.question.order+1));
      if(element!=null)
  		{
        element.scrollIntoView({behavior:"smooth"});
      }else{
        //TODO: scroll vers le bouton Valider
      }
  	});

  	//Si la question a une dépendance
  	if(this.question.dependance.key!=''){	
  		//On récupère la value de la question dont elle dépend
	  	this.form.get(this.question.dependance.key).valueChanges.subscribe((value)=>{
        //Si la réponse n'est pas la réponses négative
	  		if(value!=this.question.dependance.value)
	  			{
	  				if(this.labeltemporaire!=''){
		  			//Remplace le nom de l'enseigne en cas de changement
			  		this.question.label=this.question.label.replace(this.labeltemporaire,value);
			  		this.labeltemporaire=value;
		  		}else{
		  			//Remplace le 'REC' du label de la question
		  			this.question.label=this.question.label.replace(this.question.dependance.label,value);
		  			this.labeltemporaire=value;
		  		}
	  		}
	  	});
  	}
  }
}