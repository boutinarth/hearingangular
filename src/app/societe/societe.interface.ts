export interface Societe{
	id: Number;
	nom : String;
	type_abonnement : Number;
	date_facturation : Date;
}