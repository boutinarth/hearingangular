export interface Questionnaire{
	id: Number,
	id_societe : Number,
	id_pointvente : Number,
	themes : string,
	questions : string
}